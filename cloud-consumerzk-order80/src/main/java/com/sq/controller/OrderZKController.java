package com.sq.controller;

import com.sq.entities.CommonResult;
import com.sq.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class OrderZKController {

   // public static final String INVOKE_URL = "http://cloud-provider-payment";
    public static final String INVOKE_URL = "http://cloud-consul-payment";

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping(value = "/consumer/payment/zk")
    public String getZK(){
        String forObject = restTemplate.getForObject(INVOKE_URL + "/payment/zk", String.class);
        return forObject;

    }
}
