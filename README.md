## 完整项目地址 https://github.com/acloudyh/springCloud.git
## 修改idea 显示多个进程 在 .idea目录下的 workspace.xml
    <component name="RunDashboard">
    <option name="configurationTypes">
          <set>
            <option value="SpringBootApplicationConfigurationType" />
          </set>
    </option>
    </component>
## 本地安装 zookeeper
    brew install zookeeper
    zkServer start/stop
    
## Consul （https://www.springcloud.cc/spring-cloud-consul.html）
    1 简介：consul是一套分布式的服务发现和配置管理的系统（Go语言开发）
    2 consul agent -dev 启动 【访问 localhost：8500】
    
## eureka zookeeper consul三种注册中心的异同点
    | 组件名称 | 语言 | CAP | 服务健康检查 | 对外暴露接口 | spring cloud 集成 |
      eureka   Java   AP    可配置        HTTP         true
      consul   Go     CP    支持        HTTP/DNS      true
      zookeeper Java  CP    支持         客户端        true
      
## 网关三大核心 路由 断言 过滤器

## mac 安装mq
     brew install rabbitmq -- MQ的安装目录在 /usr/local/Cellar/rabbitmq
    
    安装RabiitMQ的可视化监控插件
       // 切换到MQ目录,注意你的安装版本可能不是3.7.4
       cd /usr/local/Cellar/rabbitmq/3.7.4/
       // 启用rabbitmq management插件
       sudo sbin/rabbitmq-plugins enable rabbitmq_management
    
    配置环境变量
     sudo vi /etc/profile
     //加入以下两行
     export RABBIT_HOME=/usr/local/Cellar/rabbitmq/版本号
     export PATH=$PATH:$RABBIT_HOME/sbin
     // 立即生效
     source /etc/profile
   
    后台启动mq
     rabbitmq-server -detached  【brew services start rabbitmq】
       // 查看状态
       rabbitmqctl status 
       // 访问可视化监控插件的界面
       // 浏览器内输入 http://localhost:15672,默认的用户名密码都是guest,登录后可以在Admin那一列菜单内添加自己的用户
       rabbitmqctl stop 关闭
## spring cloud stream 
    1 启动两个StreamMQMain8802 修改一下端口（8802，8803） 此时 8801发送的消息会被重复消费【运用分组和持久化的方式解决该问题】
        在stream中处于同一个group中的多个消费者是竞争关系，这样能保证一个消息只被一个消费一次；不同组是可以重复消费的       
        配置 group 同样的表示是同组 这样就不会重复消费了
        
## springcloud sleuth + zipkin 调用链路监控 

## SpringCloud Alibaba 【https://github.com/alibaba/spring-cloud-alibaba/blob/master/README-zh.md】
    1 Nacos 服务注册和配置中心 【 Nacos == Eureka + Config + Bus】
    