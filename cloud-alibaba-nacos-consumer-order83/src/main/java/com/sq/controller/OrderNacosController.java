package com.sq.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@RestController
public class OrderNacosController {

    @Value("${service-url.nacos-user-service}")
    private String SERVER_URL;

    @Resource
    RestTemplate restTemplate;

    @GetMapping("consumer/payment/nacos/{id}")
    public String req(@PathVariable("id") Integer id){
        String s = restTemplate.getForObject(SERVER_URL+"/payment/nacos/"+id, String.class);
        System.out.println("result : "+ s +" \t from nacos provider....");
        return s;
    }

}
