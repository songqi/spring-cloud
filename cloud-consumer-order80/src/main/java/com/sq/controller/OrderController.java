package com.sq.controller;

import com.sq.entities.CommonResult;
import com.sq.entities.Payment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@RestController
@Slf4j
public class OrderController {

    public static final String PAYMENT_URL = "http://127.0.0.1:8001";

    @Resource
    private RestTemplate restTemplate;

    @GetMapping("/consumer/payment/create")
    public CommonResult<Payment> create(Payment payment){
       return (CommonResult)restTemplate.postForObject(PAYMENT_URL + "/payment/create", payment, CommonResult.class);
    }

    @GetMapping("/consumer/payment/find/{id}")
    public CommonResult<Payment> create(@PathVariable("id") Long id){
        return (CommonResult)restTemplate.getForObject(PAYMENT_URL + "/payment/find/"+ id, CommonResult.class);
    }


}
