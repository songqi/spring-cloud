package com.sq.service;

public interface IMessageProvider {
    String send();
}
