package com.sq.controller;

import com.sq.entities.CommonResult;
import com.sq.entities.Payment;
import com.sq.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@Slf4j
public class PaymentController {

    @Resource
    PaymentService paymentService;

    @PostMapping(value = "/payment/create")
    public CommonResult<Integer> create(@RequestBody Payment payment) { // ***不加这个@RequestBody的话 远程调用该接口 插入数据库的数据是空的
        int i = paymentService.create(payment);
        if (i > 0) {
            return new CommonResult(200, "insert success ...", i);
        }
        return new CommonResult(400, "insert fail ...", null);
    }

    @GetMapping(value = "/payment/get/{id}")
    public CommonResult<Payment> find(@PathVariable("id") Long id) {
        // 编译器报错可忽视
        //log.info("run find");
        Payment payment = paymentService.findById(id);

        if (payment != null) {
            return new CommonResult(200, "find success ...", payment);
        }
        return new CommonResult(400, "find fail ...", null);
    }
}
