package com.sq.service;

import com.sq.entities.Payment;
import org.apache.ibatis.annotations.Param;

public interface PaymentService {
    int create(Payment payment);

    Payment findById(@Param("id") Long id);
}
