package com.sq.service.impl;

import com.sq.dao.PaymentDao;
import com.sq.entities.Payment;
import com.sq.service.PaymentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Resource
    PaymentDao paymentDao;

    public int create(Payment payment) {
        return paymentDao.create(payment);
    }

    public Payment findById(Long id) {
        return paymentDao.findById(id);
    }
}
